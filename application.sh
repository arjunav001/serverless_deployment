#!/bin/bash 
# install the serverless framework CLI
npm install -g serverless
serverless config credentials --provider aws --key AKIA4U6OOKPIIB5QMX67 --secret  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
# create a new service using the CLI's AWS Node.js template
serverless create --template aws-nodejs --path my-awesome-service

# using Python?
# serverless create --template aws-python3 --path my-awesome-service

# switch to the project directory and install the Datadog plugin
cd my-awesome-service
npm install --save-dev serverless-plugin-datadog
npm install --save-dev datadog-lambda-js
# using Yarn?
# yarn add --dev serverless-plugin-datadog
cat << EOF >> serverless.yml
custom:
  datadog:
    addLayers: true
    apiKey: "e3fdfdd371ee80235894bbde86f7c548"
    flushMetricsToLogs: false

EOF
>handler.js 
cat << EOF >> handler.js
const { datadog, sendDistributionMetric } = require("datadog-lambda-js");

async function hello(event, context) {
  sendDistributionMetric(
    "coffee_house.order_value",       // Metric name
    12.45,                            // Metric value
    "product:latte", "order:online"   // Associated tags
  );
  return {
    statusCode: 200,
    body: "hello, dog!",
  };
}

module.exports.hello = datadog(hello);

EOF

cd   /home/ubuntu/my-awesome-service
sudo serverless deploy --region us-east-1 --stage production    
sudo serverless deploy --region us-east-1 -f hello --stage production  
sudo serverless invoke -f hello   --path /home/ubuntu/my-awesome-service