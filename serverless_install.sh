#!/bin/bash
node --version > /dev/null
if [ $? -ne 0 ]
then
	curl -sL https://deb.nodesource.com/setup_10.x -o nodesource_setup.sh
	sudo bash nodesource_setup.sh
	sudo apt install nodejs -y
	node --version
else 
	echo "nodejs is already installed on system.......!"
fi 
serverless -v > /dev/null
if  [ $? -ne 0 ]
then
	sudo npm install serverless -g
	serverless -v
else 
	echo "serverless framework is already installed on system....!"
fi 
